# Kneading Orbital Graph

This pacakge provides an implementation for computing and vizualizing Kneading Orbital Graph

## Installation

This package works with Python 3.6-3.8

You can install latest stable version from [PyPI](https://pypi.org/):

```
$ pip3 install kneading-orbital-graph
```

To install the latest development version:

```
$ pip3 install -e git+https://gitlab.com/dmartinez05/kneading_orbital_graph.git@master#egg=kneading_orbital_graph
```

## Documentation

The documentation for the package is available at [Manual](https://gitlab.com/dmartinez05/kneading_orbital_graph/-/blob/master/manual.pdf)
